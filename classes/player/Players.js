class Players {
    constructor() {
        this.players = [];
    }

    push(player) {
        try {
            this.indexOf(player.id);
            throw new Error(`Player with id: ${id} already exists`);
        } catch (err) {
            this.players.push(player);
        }
    }

    indexOf(id) {
        id = Number(id);
        for (let i = 0; i < this.players.length; i++) {
            if (this.players[i].id === id) {
                return i;
            }
        }
        return -1;
    }

    getPlayerById(id) {
        var index = this.indexOf(id);
        if (index < 0) {
            throw new Error(`(Get) Player with id: ${id} not found!`);
        }
        return this.players[index];
    }
}

exports.Players = Players;