var id = 1;

class Player {
    constructor(nick) {
        this.id = id++;
        this.nick = nick;
        this.socket = null;
        this.lobbyId = -1;
        this.gameId = null;
    }
    
    changeLobby(newLobbyId) {
        if (this.lobbyId >= 0) {
            try {
                lobbies.getLobbyById(this.lobbyId).removePlayerById(this.id);
            } catch (err) {
                logger.error(err.stack);
            }
        }
        this.lobbyId = newLobbyId;
    }

    updateNick(newNick) {
        try {
            this.nick = newNick;
            lobbies.getLobbyById(this.lobbyId).getPlayerById(this.id).nick = newNick;

        } catch (err) {
            logger.error(err.stack);
        }
    }

}

exports.Player = Player;