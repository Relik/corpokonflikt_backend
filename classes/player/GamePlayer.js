const Player = require('./Player').Player;

class GamePlayer extends Player {
    constructor (player, gameId) {
        super(player.nick);
        this.nick = player.nick;
        this.id = player.id; ///@INFO: Not sure if it should be same id :/
        this.gameId = gameId;
        this.x = 0;
        this.y = 0;
        this.isAlife = true;
        this.collor = "";
    }

}

exports.GamePlayer = GamePlayer;