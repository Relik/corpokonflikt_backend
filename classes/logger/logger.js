var date = new Date();
var dateTime = ""
    + date.getDate() + "-"
    + (date.getMonth() + 1) + "-"
    + date.getFullYear() + "__"
    + date.getHours() + ";"
    + date.getMinutes();
const opts = {
    // logFilePath: `./logFiles/Log_${dateTime}.log`,
    timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
};

const Logger = require('simple-node-logger').createSimpleLogger(opts);
Logger.info = function (msg) {
    Logger.log('info', msg);
}
Logger.error = function (err) { //@TODO: var if print stack trace
    Logger.log('error', err);
}
Logger.debug = function (msg) {
    Logger.log('debug', msg);
}
exports.Logger = Logger;
