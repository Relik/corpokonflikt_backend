const Game = require('./Game').Game;

class Games {
    constructor() {
        this.games = [];
    }

    push(game){
        this.games.push(game);
    }

    indexOf(id) {
        id = Number(id);

        for (let i = 0; i < this.games.length; i++) {
            if (this.games[i].id === id) {
                return i;
            }
        }
        return -1;
    }

    getGameById(id) {
        var index = this.indexOf(id);
        if (index < 0) {
            throw new Error(`(Get) Game with id: ${id} not found!`);
        }
        return this.games[index];
    }
}

exports.Games = Games;