const GamePlayer = require('../player/GamePlayer').GamePlayer;
const ytDownloader = require('ytdl-core');
const path = require('path');
const fs = require('fs');
var id = 1;

class Game {
    constructor(lobby) {
        this.id = id++;
        this.players = [];
        this.name = lobby.name;
        this.songTitle = lobby.songTitle;
        this.downloadSong(lobby.songInfo);
        lobby.players.forEach(player => {
            this.players.push(new GamePlayer(player, this.id));
        });

    }

    indexOf(id) {
        id = Number(id);

        for (let i = 0; i < this.players.length; i++) {
            if (this.players[i].id === id) {
                return i;
            }
        }
        return -1;
    }

    downloadSong(info) {
        if (!info) {
            throw new Error(`Info for game undefined`);
        }
        var songFile = new fs.createWriteStream(path.join(ROOT_DIRECTORY, process.env.SONG_DIRECTORY, `${this.songTitle}.mp3`))
        ytDownloader.downloadFromInfo(info, { format: 'mp3', filter: "audioonly" })
            .pipe(songFile);
        songFile.on('finish', () => {
            logger.info(`Song downloaded: ${this.songTitle}`);
            ioSocket.emit(`gameStarted${lobby.id}`, this);
        });
    }

    getPlayerById(id) {
        var index = this.indexOf(id);
        if (index < 0) {
            throw new Error(`(Get) Player with id: ${id} not found in game with id: ${this.id}!`);
        }
        return this.players[index];
    }
    //@TODO Zwiększyć przebytą odległość
    updatePlayer(player) {
        var index = this.indexOf(player.id);
        if (index < 0) {
            throw new Error(`(Get) Player with id: ${player.id} not found in game with id: ${this.id}!`);
        }
        this.players[index] = player;
        this.sendStatusToPlayers();
    }

    sendStatusToPlayers() {
        ioSocket.emit(`gameUpdate${this.id}`, this);
        return;
    }

}

exports.Game = Game;