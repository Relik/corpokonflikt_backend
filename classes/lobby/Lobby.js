const Room = require('./Room').Room;
const ytDownloader = require('ytdl-core');
const fs = require('fs');
class Lobby extends Room {
    constructor(room) {
        var host = players.getPlayerById(room.host);
        super(
            room
        );
        this.host = host;
        this.songName = null;
        this.players = [];
        this.addPlayer(this.host);
        this.songInfo = {
            title: null
        }
    };

    getSongInfo(url) {
        ytDownloader.getInfo(url,{format: 'mp3', filter: "audioonly"})
            .then(info => {
                if (info.length_seconds > 300) {
                    this.songTitle = 'Video too long!'; //@TODO: download only 5 minutes of video, may be impossilbe
                } else {
                    /**
                     * Removing special characters 
                     * cause you can not save file that includes them.
                     */
                    this.songTitle = info.title.replace(/[&\/\\#":*?<>|]/g, '') + "_" + this.id;
                    this.songInfo = info;
                }
                this.sendStatusToPlayers();
            })
            .catch(err => {
                logger.error(err.stack);
            })
    }

    sendStatusToPlayers() {
        ioSocket.emit(`lobbyUpdate${this.id}`, this)
        return;
    }

    update(lobby) {
        this.setRoom(lobby);
        this.getSongInfo(this.url);
        this.sendStatusToPlayers();
        return this;
    }

    addPlayer(player) {
        if (this.maxPlayers <= this.players.length)
            throw new Error(`Cannot add player ${player.name} with id: ${player.id}, lobby is full`);
        /**
         * Player already is in lobby
         */
        if (this.indexOfPlayer(player.id) >= 0) {
            return;
        }
        this.players.push(player);
        players.getPlayerById(player.id).changeLobby(this.id);
        this.host = this.host || player;
        this.playersAmount = this.players.length;
        this.sendStatusToPlayers();
    }

    indexOfPlayer(id) {
        id = Number(id);
        for (let i = 0; i < this.players.length; i++) {
            if (this.players[i].id === id)
                return i;
        }
        return -1;
    }

    getPlayerById(id) {
        var index = this.indexOfPlayer(id);
        if (index < 0) {
            throw new Error(`(Get) Player with id: ${id} not found in lobby: ${this.name}, id: ${this.id}`);
        }
        return this.players[index];
    }
    
    removePlayerById(id) {
        var index = this.indexOfPlayer(id);
        if (index < 0) {
            throw new Error(`(Remove) Player with id: ${id} not found in lobby: ${this.name}, id: ${this.id}`);
        }
        this.players.splice(index, 1);
        this.playersAmount = this.players.length;
        if (id === this.host.id) {
            if (this.playersAmount === 0) {
                this.host = null;
            } else {
                this.host = this.players[0];
            }
        }
        players.getPlayerById(id).changeLobby(-1);
        this.sendStatusToPlayers();
        return this;
    }
}

exports.Lobby = Lobby;