var id = 1;
class Room {
    constructor(object) {
        this.id = id++; //@TODO: Add complicated id generator
        this.setRoom(object);
        this.playersAmount = 0;
    }
    
    setRoom(object) {
        object.name = object.name || `Room#${id}`;
        this.name = object.name.substring(0, 25);
        this.url = object.url;
        this.access = object.access;
        this.type = object.type;
        this.maxPlayers = object.maxPlayers;

    }

    generateBriefInfo() {
        return {
            id: this.id,
            name: this.name,
            access: this.access,
            type: this.type,
            playersAmount: this.playersAmount,
            maxPlayers: this.maxPlayers
        };
    }
}

exports.Room = Room;