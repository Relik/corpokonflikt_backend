const Lobby = require('./Lobby').Lobby;

class Lobbies {
    constructor() {
        this.lobbies = [];
    }

    push(lobby) {
        this.lobbies.push(lobby);
    }

    indexOf(id) {
        id = Number(id);
        for (let i = 0; i < this.lobbies.length; i++) {
            if (this.lobbies[i].id === id) {
                return i;
            }
        }
        return -1;
    }

    getLobbyById(id) {
        var index = this.indexOf(id);
        if (index < 0) {
            throw new Error(`(Get) Lobby with id: ${id} not found!`);
        }
        return this.lobbies[index];
    }

    getBriefInfo() {
        let briefLobbies = [];
        for (let i = 0; i < this.lobbies.length; i++) {
            briefLobbies.push(this.lobbies[i].generateBriefInfo());
        }
        return briefLobbies;
    }

    removeLobbyById(id) {
        var index = this.indexOf(id);
        if (index < 0) {
            throw new Error(`(Remove) Lobby with id: ${id} not found!`);
        }
        this.lobbies.splice(index, 1);
    }
};

exports.Lobbies = Lobbies;

