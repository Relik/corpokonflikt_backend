var gameController = require('../controllers/gameController');

var socket = (server) => {
  var ioSocket = require('socket.io')(server);

  ioSocket.on('connection', (player) => {

    player.on('subscribe', (playerId, lobbyId) => { //@TODO: remove player from lobby when connection closed
      logger.info(`Registered new socket player, id: ${playerId}`);
    });

    player.on('updatePlayerStatusInGame', (gamePlayer) => {
      try {
        gameController.updatePlayerStatusInGame(gamePlayer);
      } catch (err) {
        logger.error(err.stack);
        player.disconnect();
      }
    });
    
  });
  return ioSocket;
}


exports.ioSocket = socket;