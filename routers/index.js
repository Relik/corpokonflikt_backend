const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const app = express();
const path = require('path')
const fs = require('fs');
const mkdirp = require('mkdirp')

const Lobbies = require('../classes/lobby/Lobbies').Lobbies;
const Players = require('../classes/player/Players').Players;
const Games = require('../classes/game/Games').Games;
var gameRouter = require('./gameRouter').gameRouter;
var lobbyRouter = require('./lobbyRouter').lobbyRouter;
var playerRouter = require('./playerRouter').playerRouter;
var rootRouter = require('./rootRouter').rootRouter;

ROOT_DIRECTORY = path.join(path.dirname(require.main.filename));

players = new Players();
lobbies = new Lobbies();
games = new Games();

// fs.unlink('songs/*', (err) => {
//     if (err) throw err;
//     console.log('path/file.txt was deleted');
//   });
// ps:scale web=1
mkdirp(path.join(ROOT_DIRECTORY, process.env.SONG_DIRECTORY), function (err) {
    if (err)
        logger.error(err.stack);
    fs.readdir(path.join(ROOT_DIRECTORY, process.env.SONG_DIRECTORY), (err, files) => {
        if (err) throw err;
        for (const file of files) {
            fs.unlink(path.join(ROOT_DIRECTORY, process.env.SONG_DIRECTORY, file), err => {
                if (err) throw err;
            });
        }
    });
});
// app.use(bodyParser.urlencoded({
//     limit: '50mb',
//     extended: true
// }));
// app.use(function (req, res, next) {
//     res.setHeader('Access-Control-Allow-Origin', `*`);
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//     res.setHeader('Access-Control-Allow-Credentials', true);
//     next();
// });
var whitelist = ['https://cavenauts.herokuapp.com', 'http://localhost:3000', 'http://localhost:3001']
var corsOptions = {
    origin: function (origin, callback) {
        if (whitelist.indexOf(origin) !== -1 || !origin) {
            callback(null, true)
        } else {
            callback(new Error(`Not allowed by CORS origin: ${origin}`));
        }
    }
}
app.use(cors(corsOptions));
app.use(bodyParser.json({ limit: '50mb', extended: true }));
app.use(express.static(path.join(ROOT_DIRECTORY, 'build')));
app.use('/lobby', lobbyRouter);
app.use('/game', gameRouter);
app.use('/player', playerRouter);
app.use('/', rootRouter);
exports.app = app;
