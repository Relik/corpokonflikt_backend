var router = require('express').Router();
var lobbyController = require('../controllers/lobbyConroller');

router.get('/getLobbies', lobbyController.getLobbies);
router.post('/create', lobbyController.create);
router.post('/:id/removePlayer', lobbyController.removePlayer);
router.post('/:id/join', lobbyController.addPlayer);
router.get('/:id/getInfo', lobbyController.getInfo);
router.post('/:id/update', lobbyController.updateLobby);

exports.lobbyRouter = router;
 