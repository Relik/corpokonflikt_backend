var router = require('express').Router();
var rootController = require('../controllers/rootController');

router.get('/', rootController.mainSite);

exports.rootRouter = router;
