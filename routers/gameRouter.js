var router = require('express').Router();
var gameController = require('../controllers/gameController');

router.get('/:id/getInfo', gameController.getInfo);
router.get('/:id/getSong', gameController.getSong);
router.post('/create', gameController.create);

exports.gameRouter = router;