var router = require('express').Router();
var playerController = require('../controllers/playerController');

router.post('/register', playerController.registerPlayer);

exports.playerRouter = router;