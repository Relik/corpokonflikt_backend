const Game = require('../classes/game/Game').Game;
const path = require('path');
exports.updatePlayerStatusInGame = function (gamePlayer) {
    games.getGameById(gamePlayer.gameId).updatePlayer(gamePlayer);
    //logger.info(`Player: ${gamePlayer.name}, with id: ${gamePlayer.id} succsessfully updated`);
}

exports.create = function (req, res) {
    var lobby = req.body;
    logger.info(`Creating new game: ${lobby.name}`);
    try {
        var newGame = new Game(lobby);
    } catch (err) {
        logger.error(err.stack);
        res.status(500).send(err);
        return;
    }
    games.push(newGame);
    logger.info(`Game created: ${newGame.name}, ${newGame.id}`);
    lobbies.removeLobbyById(lobby.id);
    res.send(newGame);
}

exports.getSong = function (req, res) {
    var id = req.params.id;
    logger.info(`Was asked about song in game: ${id}`);
    try {
        var songTitle = games.getGameById(id).songTitle;
        logger.info(`Sending song in game: ${id}, title: ${songTitle}`);
        res.sendFile(path.join(ROOT_DIRECTORY, process.env.SONG_DIRECTORY, `${songTitle}.mp3`));
    } catch (err) {
        logger.error(err.stack);
        res.status(500).send(err);
    }
}

exports.getInfo = function (req, res) {
    var id = req.params.id;
    logger.info(`Sending info about lobby, with id: ${id}`);

    try {
        let lobby = lobbies.getLobbyById(id);
        res.send(lobby);
        logger.info(`Lobby: ${lobby.name}, with id: ${lobby.id} succsessfully send`);
    } catch (err) {
        logger.error(err.stack);
        res.status(404)
            .send(err);
    }
}