const Lobby = require('../classes/lobby/Lobby').Lobby;

exports.getLobbies = function (req, res) {
    logger.info(`Was asked about lobbies(${lobbies.lobbies.length})`);
    res.send(lobbies.getBriefInfo());
};

exports.create = function (req, res) {
    var lobby = req.body;
    logger.info(`Creating lobby: ${lobby.name}`);

    var newLobby = new Lobby(lobby);
    lobbies.push(newLobby);
    logger.info(`Lobby created: ${newLobby.name}, ${newLobby.id}`);

    res.send(newLobby);
};

exports.updateLobby = function (req, res) {
    var id = req.params.id;
    lobby = req.body;
    logger.info(`Updading lobby: ${lobby.name} with id: ${id}`);

    try {
        res.send(lobbies.getLobbyById(id).update(lobby))
        logger.info(`Lobby: ${lobby.name}, with id: ${id} succsessfully updated`);
    } catch (err) {
        logger.error(err.stack);
        res.status(404)
            .send(err);
    }
}
exports.getInfo = function (req, res) {
    var id = req.params.id;
    logger.info(`Sending info about lobby, with id: ${id}`);

    try {
        let lobby = lobbies.getLobbyById(id);
        res.send(lobby);
        logger.info(`Lobby: ${lobby.name}, with id: ${lobby.id} succsessfully send`);
    } catch (err) {
        logger.error(err.stack);
        res.status(404)
            .send(err);
    }
}

exports.addPlayer = function (req, res) {
    var playerId = req.body.id;
    var lobbyId = req.params.id;
    logger.info(`Adding player with id: ${playerId} to lobby with id: ${lobbyId}`);
    try {
        var player = players.getPlayerById(playerId);
        lobbies.getLobbyById(lobbyId).addPlayer(player);
        res.send(lobbies.getLobbyById(lobbyId));
        logger.info(`Succsessfully added player with id: ${playerId} to lobby with id: ${lobbyId}`);

    } catch (err) {
        logger.error(err.stack);
        res.status(500)
            .send(err);
    }
}

exports.removePlayer = function (req, res) {
    var lobbyId = req.params.id;
    var playerId = req.body.id;
    res.send(lobbies.getLobbyById(lobbyId).removePlayerById(playerId));
}
