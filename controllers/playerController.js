const Player = require('../classes/player/Player').Player;

exports.registerPlayer = function (req, res) {
    var player = req.body;
    logger.info(`Adding player: ${player.nick}`);
    /**
     * Check if player is in array of players.
     */
    var index = players.indexOf(player.id);
    try {
        if (index >= 0) { //@TODO: better always give new id and delete players that are no longer playing
            logger.info(`Player: ${player.nick}, id: ${player.id} already exists, only changing name.`);
            players.getPlayerById(player.id).updateNick(player.nick);
            res.status(200).send(player);
            return;
        } else {
            player = new Player(player.nick);
            players.push(player);
            res.send(player);
            logger.info(`Successfully added new player ${player.nick}, id: ${player.id}`)
        }
    } catch (err) {
        logger.error(err.stack);
        res.status(500)
            .send(err);
    }

};
