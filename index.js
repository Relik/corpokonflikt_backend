require('dotenv').load();
var app = require('./routers/index').app;
var server = require('http').createServer(app);
ioSocket = require('./socket/socketEventSubscriber').ioSocket(server);
logger = require('./classes/logger/logger').Logger;

const PORT = process.env.PORT ? process.env.PORT : 3001;

server.listen(PORT, () => {
    logger.info(`Hello, I\'m listening on PORT:${PORT}`)
});